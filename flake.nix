{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem(system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          (self: super: {
            speechd = super.speechd.override {
              withFlite = false;
              withPico = false;
            };
            qgroundcontrol = super.qgroundcontrol.overrideAttrs (old: {
              buildInputs = [ super.SDL2 ] ++ old.qtInputs ++ (with super.gst_all_1; [
                gstreamer
                gst-plugins-base
                (gst-plugins-good.override { qt5Support = true; })
                gst-plugins-bad
                #gst-plugins-ugly
                gst-libav
                pkgs.wayland
              ]);
            });
          })
        ];
      };
    in {
      devShell = let
        pythonEnv =
          (pkgs.python3.withPackages (python-packages: with python-packages; [
            pyyaml
            pygame
          ]));
      in pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          mavproxy
          qgroundcontrol
          speechd
          kicad
        ];
        shellHook = ''
          PYTHONPATH=${pythonEnv}/${pythonEnv.sitePackages}
        '';
      };
    });
}
